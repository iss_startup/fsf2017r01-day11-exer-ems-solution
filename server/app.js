// TODO: Retrieve (retrieve all and eager loading) and insert data from the database.
// TODO: 1. Install and load sequelize; install but do not load mysql
// TODO: 2. Set up MySQL connection
// TODO: 3. Define and load models
// TODO: 4. Persist registration information

// TODO: 9. Retrieve employee information based on queryString passed. Match queryString to both employee name
// TODO: 9. employee number
// TODO: 10. Perform eager loading so that the returned data includes the employee manager id and name

// DEPENDENCIES ------------------------------------------------------------------------------------------------------
// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");

// TODO: 1.1 Load sequelize, assign it to a variable called Sequelize
// Loads sequelize ORM
var Sequelize = require("sequelize");

// CONSTANTS ---------------------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

// TODO: 2.1 Define your MYSQL username and password as constants
// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'root';


// OTHER VARS ---------------------------------------------------------------------------------------------------------
//Create an instance of express application
var app = express();


// MIDDLEWARES --------------------------------------------------------------------------------------------------------
// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// Populates req.body with information submitted through the registration form.
// Expected content type is application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({extended: false}));
// Default $http content type is application/json so we use json as the parser type
app.use(bodyParser.json());


// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
//TODO :2.2 Create a connection to MySQL DB
// Creates a MySQL connection
var sequelize = new Sequelize(
    'employees',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         // default port    : 3306
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// TODO: 3.3 Load employees table model
var Employee = require('./models/employees')(sequelize, Sequelize);

// TODO: 9.1 Load departments and dept_emp table models
var Department = require('./models/departments')(sequelize, Sequelize);
var DeptEmp = require('./models/deptemp')(sequelize, Sequelize);

// TODO: 9.2 Define associations
Employee.hasMany(DeptEmp, {foreignKey: 'emp_no'})
// DeptEmp.hasOne(Department, {foreignKey: 'dept_no'});
DeptEmp.belongsTo(Department, {foreignKey: 'dept_no'});


// ROUTE HANDLERS -----------------------------------------------------------------------------------------------------
// Defines endpoint exposed to client side for registration
app.post("/api/employees", function (req, res, next) {
    // Information sent via an HTTP POST is found in req.body
    console.log('\nInformation submitted to server:')
    console.log(req.body);

    // TODO: 4.1 Persist (save) registration information using sequelize
    Employee
        .create({
            emp_no: req.body.emp.empNo,
            birth_date: new Date(req.body.emp.birthday),
            first_name: req.body.emp.firstname,
            last_name: req.body.emp.lastname,
            gender: req.body.emp.gender,
            hire_date: new Date(req.body.emp.hiredate)
        })
        .then(function (employee) {
            res
                .status(200)
                .json(employee);
        })
        .catch(function (err) {
            console.log(err);
            res
                .status(501)
                .json(err);
        });
});

// TODO: 9.3 Retrieve the first 100 employee records that match query string passed. Match against emp name and emp no
// Defines endpoint handler exposed to client side for retrieving employee information from database. Client side
// sent data as part of the query string, we access query string paramters via the req.query property
app.get("/api/employees", function (req, res) {
    Employee
    // findAll asks sequelize to retrieve multiple records (all records if where clause not used, i.e., no filtering)
        .findAll({
            where: {
                // This where condition filters the findAll result so that it only includes employee names and
                // employee numbers that have the searchstring as a substring (e.g., if user entered 's' as search
                // string, the following
                $or: [
                    {first_name: {$like: "%" + req.query.searchString + "%"}},
                    {last_name: {$like: "%" + req.query.searchString + "%"}},
                    {emp_no: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
            // We add a limit since employees table is big
            , limit: 100
        })
        .then(function (employees) {
            res
                .status(200)
                .json(employees);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


/* TODO: 9.4 Retrieve the first 100 employee records that match query string passed. Match against employee name and
 employee no. Include  department information */

/* Defines endpoint handler exposed to client side for retrieving employee records that match query string passed.
 Match against dept name and dept no. Includes manager information. Client side sent data as part of the query
 string, we access query string paramters via the req.query property
*/
app.get("/api/employees/departments", function (req, res) {
    Employee
    // Use findAll to retrieve multiple records
        .findAll({
            // Use the where clause to filter final result; e.g., when you only want to retrieve employees that have
            // "s" in its name
            where: {
                // $or operator tells sequelize to retrieve record that match any of the condition
                $or: [
                    // $like + % tells sequelize that matching is not a strict matching, but a pattern match
                    // % allows you to match any string of zero or more characters
                    {first_name: {$like: "%" + req.query.searchString + "%"}},
                    {last_name: {$like: "%" + req.query.searchString + "%"}},
                    {emp_no: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
            , limit: 100
            // What Include attribute does: Join two or more tables. In this instance:
            // 1. For every Employee record that matches the where condition, the include attribute returns
            // ALL employees that have served as managers of said Employee
            // 2. model attribute specifies which model to join with primary model
            // 3. order attribute specifies that the list of Managers be ordered from latest to earliest manager
            // 4. limit attribute specifies that only 1 record (in this case the latest manager) should be returned
            , include: [{
                model: DeptEmp
                , order: [["to_date", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
                , include: [Department]
            }]
        })
        // this .then() handles successful findAll operation
        // in this example, findAll() used the callback function to return employees
        // we named it employeess, but this object also contains info about the
        // latest department of that employee
        .then(function (employees) {
            res
                .status(200)
                .json(employees);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


// Defines endpoint exposed to client side for retrieving all employee information (STATIC)
app.get("/api/static/employees", function (req, res) {
    var employees = [
        {
            empNo: 1001,
            empFirstName: 'Emily',
            empLastName: 'Smith',
            empPhoneNumber: '6516 2093'

        }
        , {
            empNo: 1002,
            empFirstName: 'Varsha',
            empLastName: 'Jansen',
            empPhoneNumber: '6516 2093'
        }
        ,
        {
            empNo: 1003,
            empFirstName: 'Julie',
            empLastName: 'Black',
            empPhoneNumber: '6516 2093'
        }
        , {
            empNo: 1004,
            empFirstName: 'Fara',
            empLastName: 'Johnson',
            empPhoneNumber: '6516 2093'
        }
        ,
        {
            empNo: 1005,
            empFirstName: 'Justin',
            empLastName: 'Zhang',
            empPhoneNumber: '6516 2093'
        }
        , {
            empNo: 1006,
            empFirstName: 'Kenneth',
            empLastName: 'Black',
            empPhoneNumber: '6516 2093'
        }

    ];
    // TODO: 10.3 Return the employees information as a json object
    // Return employees as a json object
    res
        .status(200)
        .json(employees);
});


// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});